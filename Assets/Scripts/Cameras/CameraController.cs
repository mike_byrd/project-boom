﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.InputSystem;

public class CameraController : NetworkBehaviour
{
    [SerializeField] private Transform playerCameraTransform;
    [SerializeField] private float speed;
    [SerializeField] private float screenBoarderThickness = 10f; // edge screen panning
    [SerializeField] private Vector2 screenXLimits = Vector2.zero;
    [SerializeField] private Vector2 screenZLimits = Vector2.zero;
    [SerializeField] private bool useScreenEdgeMovement;
    [SerializeField] private float zoomSpeed = 1f;
    [SerializeField] private float zoomMin = 10f;
    [SerializeField] private float zoomMax = 25f;
    
    private Controls controls;
    private Vector2 previousInput;

    private float mouseScrollY;

    public override void OnStartAuthority()
    {
        playerCameraTransform.gameObject.SetActive(true);
        
        controls = new Controls();
        
        controls.Player.MoveCamera.performed += SetPreviousInput;
        controls.Player.MoveCamera.canceled += SetPreviousInput;
        controls.Player.MouseScrollY.performed += SetScroll;
        
        controls.Enable();
    }

    [ClientCallback]
    private void Update()
    {
        if (!hasAuthority || !Application.isFocused)
        {
            return;
        }

        UpdateCameraPosition();
    }

    private void UpdateCameraPosition()
    {
        var pos = playerCameraTransform.position;
        if (previousInput == Vector2.zero)
        {
            var cursorMovement = UpdateMovementUsingScreenEdge();

            pos += speed * Time.deltaTime * cursorMovement.normalized;
        }
        else
        {
            pos += speed * Time.deltaTime * new Vector3(previousInput.x, 0f, previousInput.y);
        }

        pos.x = Mathf.Clamp(pos.x, screenXLimits.x, screenXLimits.y);
        pos.z = Mathf.Clamp(pos.z, screenZLimits.x, screenZLimits.y);

        playerCameraTransform.position = pos;
    }

    private Vector3 UpdateMovementUsingScreenEdge()
    {
        if (!useScreenEdgeMovement)
        {
            return Vector3.zero;
        }
        
        var cursorMovement = Vector3.zero;
        var cursorPosition = Mouse.current.position.ReadValue();
        if (cursorPosition.y >= Screen.height - screenBoarderThickness)
        {
            cursorMovement.z += 1;
        }
        else if (cursorPosition.y <= screenBoarderThickness)
        {
            cursorMovement.z -= 1;
        }
        else if (cursorPosition.x >= Screen.width - screenBoarderThickness)
        {
            cursorMovement.x += 1;
        }
        else if (cursorPosition.x <= screenBoarderThickness)
        {
            cursorMovement.x -= 1;
        }

        return cursorMovement;
    }

    private void SetPreviousInput(InputAction.CallbackContext ctx)
    {
        previousInput = ctx.ReadValue<Vector2>();
    }
    
    private void SetScroll(InputAction.CallbackContext ctx)
    {
        if (!playerCameraTransform)
        {
            return;
        }
        
        mouseScrollY = ctx.ReadValue<float>();

        if (mouseScrollY > 0)
        {
            if (playerCameraTransform.position.y > zoomMin)
            {
                playerCameraTransform.position -= Vector3.up * zoomSpeed;
            }
        }
        else if(mouseScrollY < 0)
        {
            if (playerCameraTransform.position.y < zoomMax)
            {
                playerCameraTransform.position += Vector3.up * zoomSpeed;
            }
        }
    }
}
