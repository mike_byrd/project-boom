﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

public class Minimap : MonoBehaviour, IPointerDownHandler, IDragHandler
{
    [SerializeField] private RectTransform minimapRect;
    [SerializeField] private float mapScale = 30f;
    [SerializeField] private float offset = -6;
    
    private Transform playerCameraTransform;

    private void Update()
    {
        if (playerCameraTransform == null)
        {
            if (NetworkClient.connection == null || NetworkClient.connection.identity == null)
            {
                return;
            }
            
            playerCameraTransform = NetworkClient.connection.identity.GetComponent<RTSPlayer>().GetCameraTransform();
            
        }
    }
    
    public void OnPointerDown(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            MoveCamera();
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            MoveCamera();
        }
    }

    private void MoveCamera()
    {
        var mousePos = Mouse.current.position.ReadValue();
        if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(
            minimapRect, 
            mousePos,
            null,
            out Vector2 localPoint))
        {
            return;
        }

        var rect = minimapRect.rect;
        var x = (localPoint.x - rect.x) / rect.width;
        var y = (localPoint.y - rect.y) / rect.height;
        var lerp = new Vector2(x, y);

        var camX = Mathf.Lerp(-mapScale, mapScale, lerp.x);
        var camY = playerCameraTransform.position.y;
        var camZ = Mathf.Lerp(-mapScale, mapScale, lerp.y);
        var newCameraPos = new Vector3(camX, camY, camZ);

        playerCameraTransform.position = newCameraPos + new Vector3(0, 0, offset);
    }
}
