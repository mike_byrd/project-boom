﻿using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class TeamColorSetter : NetworkBehaviour
{
    public static readonly int baseColor = Shader.PropertyToID("_BaseColor");
    
    [SerializeField] private Renderer[] colorRenderers;

    [SyncVar(hook = nameof(HandleTeamColorUpdated))] 
    private Color teamColor;

    #region Server

    public override void OnStartServer()
    {
        var player = connectionToClient.identity.GetComponent<RTSPlayer>();
        teamColor = player.GetTeamColor();
    }

    #endregion

    #region Client

    private void HandleTeamColorUpdated(Color oldColor, Color newColor)
    {
        foreach (Renderer colorRenderer in colorRenderers)
        {
            colorRenderer.material.SetColor(baseColor, newColor);
        }
    }

    #endregion
}
