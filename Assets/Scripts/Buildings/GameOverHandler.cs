﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class GameOverHandler : NetworkBehaviour
{
    public static event Action ServerOnGameOver;
    public static event Action<string> ClientOnGameOver;
    List<UnitBase> bases = new List<UnitBase>();
    
    #region Server

    public override void OnStartServer()
    {
        UnitBase.ServerOnBaseSpawned += HandleBaseSpawned;
        UnitBase.ServerOnBaseDespawned += HandleBaseDespawned;
    }

    public override void OnStopServer()
    {
        UnitBase.ServerOnBaseSpawned -= HandleBaseSpawned;
        UnitBase.ServerOnBaseDespawned -= HandleBaseDespawned;
    }

    private void HandleBaseSpawned(UnitBase unitBase)
    {
        bases.Add(unitBase);
    }
    
    private void HandleBaseDespawned(UnitBase unitBase)
    {
        if (this == null)
        {
            return;
        }
        
        bases.Remove(unitBase);
        if (bases.Count != 1)
        {
            return;
        }

        int playerId = bases[0].connectionToClient.connectionId;

        RpcGameOver($"Player {playerId}");
        ServerOnGameOver?.Invoke();
    }

    #endregion

    #region Client

    [ClientRpc]
    private void RpcGameOver(string winner)
    {
        ClientOnGameOver?.Invoke(winner);
    }

    #endregion
}
