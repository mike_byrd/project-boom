﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.Events;

public class Unit : NetworkBehaviour
{
    [SerializeField] private int resourceCost;
    [SerializeField] private Health health;
    [SerializeField] private UnitMovement unitMovement;
    [SerializeField] private UnityEvent onSelected;
    [SerializeField] private UnityEvent onDeselected;
    [SerializeField] private Targeter targeter;

    public static event Action<Unit> ServerOnUnitSpawned;
    public static event Action<Unit> ServerOnUnitDespawned;
    
    public static event Action<Unit> AuthorityOnUnitSpawned;
    public static event Action<Unit> AuthorityOnUnitDespawned;

    public int GetResourceCost()
    {
        return resourceCost;
    }
    
    public UnitMovement GetUnitMovement()
    {
        return unitMovement;
    }
    
    public Targeter GetTargeter()
    {
        return targeter;
    }

    #region Server

    public override void OnStartServer()
    {
        ServerOnUnitSpawned?.Invoke(this);
        health.ServerOnDie += HealthOnServerOnDie;
    }

    public override void OnStopServer()
    {
        ServerOnUnitDespawned?.Invoke(this);
        health.ServerOnDie += HealthOnServerOnDie;
    }
    
    [Server]
    private void HealthOnServerOnDie()
    {
        NetworkServer.Destroy(gameObject);   
    }

    #endregion

    #region Client

    [Client]
    public void Select()
    {
        if (!hasAuthority)
        {
            return;
        }
        
        onSelected?.Invoke();
    }
    
    [Client]
    public void DeSelect()
    {
        if (!hasAuthority)
        {
            return;
        }
        
        onDeselected?.Invoke();
    }

    public override void OnStartAuthority()
    {
        AuthorityOnUnitSpawned?.Invoke(this);
    }

    public override void OnStopClient()
    {
        if (!hasAuthority)
        {
            return;
        }
        
        AuthorityOnUnitDespawned?.Invoke(this);
    }

    #endregion

    
}
