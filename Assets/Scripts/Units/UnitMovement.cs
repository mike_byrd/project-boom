﻿using System;
using Mirror;
using UnityEngine;
using UnityEngine.AI;

public class UnitMovement : NetworkBehaviour
{
    [SerializeField] private NavMeshAgent agent;
    [SerializeField] private Targeter targeter;
    [SerializeField] private float chaseRange = 10f;

    #region Server

    public override void OnStartServer()
    {
        GameOverHandler.ServerOnGameOver += ServerHandleGameOver;
    }

    public override void OnStopServer()
    {
        
        GameOverHandler.ServerOnGameOver -= ServerHandleGameOver;
    }

    private void ServerHandleGameOver()
    {
        agent.ResetPath();
    }

    [ServerCallback]
    private void Update()
    {
        Targetable target = targeter.GetTarget();
        if (target != null)
        {
            float distSquared = (target.transform.position - transform.position).sqrMagnitude;
            float rangeSquared = chaseRange * chaseRange; 
            if (distSquared > rangeSquared)
            {
                // chase
                agent.SetDestination(target.transform.position);
            }   
            else if (agent.hasPath)
            {
                //stop chasing
                agent.ResetPath();
            }
            return;
        }
        
        if (!agent.hasPath)
        {
            return;
        }
        
        if (agent.remainingDistance > agent.stoppingDistance)
        {
            return;
        }
        
        // Clear the path when you get near your stopping distance.
        agent.ResetPath();
    }

    [Command]
    public void CmdMove(Vector3 position)
    {
        ServerMove(position);
    }

    #endregion

    [Server]
    public void ServerMove(Vector3 position)
    {
        targeter.ClearTarget();
        
        bool isValidPosition = NavMesh.SamplePosition(position, out NavMeshHit hit, 1f, NavMesh.AllAreas);
        if (!isValidPosition)
        {
            return;
        }
        
        agent.SetDestination(hit.position);
    }
}
