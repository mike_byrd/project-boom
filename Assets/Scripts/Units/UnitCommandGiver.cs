﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class UnitCommandGiver : MonoBehaviour
{
   [SerializeField] private UnitSelectionHandler unitSelectionHandler;
   [SerializeField] private LayerMask layerMask;
   private Camera mainCamera;

   private void Start()
   {
      mainCamera = Camera.main;
      GameOverHandler.ClientOnGameOver += ClientHandleGameOver;
   }

   private void OnDestroy()
   {
      GameOverHandler.ClientOnGameOver -= ClientHandleGameOver;
   }

   private void ClientHandleGameOver(string winnerName)
   {
      enabled = false;
   }

   private void Update()
   {
      if (!Mouse.current.rightButton.wasPressedThisFrame)
      {
         return;
      }

      Ray ray = mainCamera.ScreenPointToRay(Mouse.current.position.ReadValue());
      if (!Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, layerMask))
      {
         return;
      }

      if (hit.collider.TryGetComponent(out Targetable target))
      {
         if (target.hasAuthority)
         {
            TryMove(hit.point);
            return;
         }

         TryTarget(target);
         return;
      }

      TryMove(hit.point);
   }

   private void TryTarget(Targetable target)
   {
      foreach (Unit selectedUnit in unitSelectionHandler.selectedUnits)
      {
         selectedUnit.GetTargeter().CmdSetTarget(target.gameObject);
      }
   }

   private void TryMove(Vector3 position)
   {
      foreach (Unit selectedUnit in unitSelectionHandler.selectedUnits)
      {
         selectedUnit.GetUnitMovement().CmdMove(position);
      }
   }
}
