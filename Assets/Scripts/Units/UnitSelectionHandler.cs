﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.InputSystem;

public class UnitSelectionHandler : MonoBehaviour
{
    [SerializeField] private LayerMask layerMask;
    [SerializeField] private RectTransform unitSelectionArea;

    private Vector2 startPosition;
    private RTSPlayer player;
    private Camera mainCamera;
    public List<Unit> selectedUnits { get; } = new List<Unit>();

    private void Start()
    {
        mainCamera = Camera.main;
        
        player = NetworkClient.connection.identity.GetComponent<RTSPlayer>();
        
        Unit.AuthorityOnUnitDespawned += HandleAuthorityUnitDespawned;
        GameOverHandler.ClientOnGameOver += ClientHandleGameOver;
    }

    private void OnDestroy()
    {
        Unit.AuthorityOnUnitDespawned -= HandleAuthorityUnitDespawned;
        GameOverHandler.ClientOnGameOver -= ClientHandleGameOver;
    }
    
    private void ClientHandleGameOver(string obj)
    {
        enabled = false;
    }

    private void HandleAuthorityUnitDespawned(Unit unit)
    {
        selectedUnits.Remove(unit);
    }

    private void Update()
    {   
        if (Mouse.current.leftButton.wasPressedThisFrame)
        {
            StartSelectionArea();
        }
        else if (Mouse.current.leftButton.wasReleasedThisFrame)
        {
            // clear selection area
            ClearSelectionArea();
        }
        else if (Mouse.current.leftButton.isPressed)
        {
            UpdateSelectionArea();
        }
        
    }

    private void StartSelectionArea()
    {
        if (!Keyboard.current.leftShiftKey.isPressed)
        {
            // Start the selection area
            foreach (Unit selectedUnit in selectedUnits)
            {
                selectedUnit.DeSelect();
            }
            
            selectedUnits.Clear();
        }
        
        startPosition = Mouse.current.position.ReadValue();
        unitSelectionArea.gameObject.SetActive(true);
        
        UpdateSelectionArea();
    }

    private void UpdateSelectionArea()
    {
        Vector2 mousePosition = Mouse.current.position.ReadValue();
        float areaWidth = mousePosition.x - startPosition.x;
        float areaHeight = mousePosition.y - startPosition.y;
        
        unitSelectionArea.sizeDelta = new Vector2(Mathf.Abs(areaWidth), Mathf.Abs(areaHeight));
        unitSelectionArea.anchoredPosition = startPosition + new Vector2(areaWidth * 0.5f, areaHeight * 0.5f);
    }

    private void ClearSelectionArea()
    {
        unitSelectionArea.gameObject.SetActive(false);

        // We clicked instead of dragged. Using .01f instead of 0 to make Rider happy with float shenanigans
        if (unitSelectionArea.sizeDelta.magnitude <= .01f) 
        {
            SelectSingleUnit();
        }
        else
        {
            SelectMultipleUnits();
        }
    }

    private void SelectMultipleUnits()
    {
        Vector2 min = unitSelectionArea.anchoredPosition - unitSelectionArea.sizeDelta * 0.5f;
        Vector2 max = unitSelectionArea.anchoredPosition + unitSelectionArea.sizeDelta * 0.5f;

        foreach (Unit unit in player.GetMyUnits())
        {
            if (selectedUnits.Contains(unit))
            {
                continue;
            }
            
            Vector3 screenPosition = mainCamera.WorldToScreenPoint(unit.transform.position);
            if (screenPosition.x > min.x && screenPosition.x < max.x
                && screenPosition.y > min.y && screenPosition.y < max.y)
            {
                selectedUnits.Add(unit);
                unit.Select();
            }
        }
    }

    private void SelectSingleUnit()
    {
        Ray ray = mainCamera.ScreenPointToRay(Mouse.current.position.ReadValue());
        if (!Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, layerMask))
        {
            return;
        }

        if (!hit.collider.TryGetComponent(out Unit unit))
        {
            return;
        }

        if (!unit.hasAuthority)
        {
            return;
        }
        
        selectedUnits.Add(unit);

        foreach (Unit selectedUnit in selectedUnits)
        {
            selectedUnit.Select();
        }
    }
}
